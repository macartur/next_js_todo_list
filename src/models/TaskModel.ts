export type TaskModel = {
    id: string;
    title?: string | null
    isCompleted: boolean
    createdAt?: Date
}