"use client"
import { useRef } from "react";

interface FromProps {
    children: React.ReactNode;
    action?: (formData: FormData) => Promise<void|boolean>;
    className?: string;
    onSubmit?: () => void;
}

export default function Form({
    children, 
    action, 
    className, 
    onSubmit
}: FromProps) {
    const ref = useRef<HTMLFormElement>(null)
    return (
        <form 
            className={className} 
            onSubmit={onSubmit}
            ref={ref}
            action={async (formData: FormData) => {
                //await action(formData)
                //ref.current?.reset()
            }}
        >
            {children}
        </form>
    );
}