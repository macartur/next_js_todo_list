"use client"

import clsx from "clsx";

interface ButtonProps {
    type?: "submit" | "button" | "reset";
    text: string | React.ReactNode;
    onClick?: () => void;
    actionButton?: boolean
}

export default function Button(
{
 type,
 text,
 onClick,
 actionButton
} : ButtonProps
){
    return (
        <button 
            type={type}
            onClick={onClick}
            className={clsx(
                actionButton && `bg-greep-700 rounded-full p-2 text-white`, 
                'bg-orange-700 px-2 text-white'
            )}
        >
            {text}
        </button>
    );
}