import Form from "../ui/Form";
import Button from "../ui/Button";
import Input from "../ui/Input";
import { TaskModel } from "@/models/TaskModel";
import { MdDelete } from "react-icons/md";
import { deleteTask } from "@/actions/todoActions";


interface DeleteTaskProps {
    task: TaskModel
}

export default function DeleteTask ({task}: DeleteTaskProps) {
    return (
        <Form action={deleteTask} >
            <Input 
                name="inputID"
                value={task.id}
                type="hidden"
            />
            <Button actionButton type="submit" text={<MdDelete />} ></Button>
        </Form>
    );
}