import { TaskModel } from "@/models/TaskModel";
import DeleteTask from "./DeleteTask";
import ChangeTask from "./ChangeTask";


interface TaskProps {
    task: TaskModel
}

export default function Task ({task}: TaskProps) {

    const taskStyle = {
        textDecoration: task.isCompleted === true ? "line-through" : "none",
        opacity: task.isCompleted === true? 0.5 : 1 
    }
    return (
        <div className="w-full flex items-center justify-between 
                        bg-white py-3 px-5 rounded-2xl"
             style={taskStyle}
        >
            <span className="text-center font-bold uppercase">
                {task.title}
            </span>
            <div className="flex justify-between w-20">
                <ChangeTask task={task} />
                <DeleteTask task={task} />
            </div>
        </div>
    );
}