"use client"
import Form from "../ui/Form";
import Button from "../ui/Button";
import Input from "../ui/Input";
import { AiOutlineCheckCircle } from "react-icons/ai";
import { changeStatus } from "@/actions/todoActions";
import { TaskModel } from "@/models/TaskModel";


interface ChangeTaskProps {
    task: TaskModel
}

export default function ChangeTask ({task}: ChangeTaskProps) {
    return (
        <Form >
            <Input 
                name="inputID"
                value={task.id}
                type="hidden"
            />
            <Button onClick={() => changeStatus(task)} actionButton type="submit" text={<AiOutlineCheckCircle />} ></Button>
        </Form>
    );
}