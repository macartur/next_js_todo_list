import AddTodo from '@/components/shared/AddTodo'
import { getTasks } from '../actions/todoActions';
import Task from '@/components/shared/Task';

export default async function Home() {

  const data = await getTasks()
  return (
    <div className="w-screen py-20 flex justify-center flex-col items-center">
      <span className='text-3xl font-extrabold uppercase'>To-do-app</span>
      <h1 className='text-3xl font-extrabold uppercase mb-5'>
        Next.js 14
        <span className='text-orange-700 ml-2'>
          Server Actions
        </span>
      </h1>

      <div className='flex justify-center flex-col w-[1000px]'>
        <AddTodo />
        <div className='flex flex-col items-center justify-center gap-5 mt-10 w-full'> 
          {
            data.map((task, id) => (
              <div className='w-full ' key={id}>
                <Task task={task} />
              </div>
            ))
          }
        </div>
      </div>
    </div>
  );
}
