"use server"
import { revalidatePath } from "next/cache"
import { prisma } from "@/utils/prisma"
import { Task } from "@prisma/client";

export async function create(formData: FormData){
    const input = formData.get("task_name") as string
    
    if (!input.trim()){
        return;
    }

    await prisma.task.create({
        data:{
            title: input
        }
    })
    revalidatePath("/")
}

export async function getTasks(){
    const tasks = await prisma.task.findMany(
        {
            select: {
                title: true,
                id: true,
                isCompleted: true,
            },
            orderBy: {
                createdAt: "desc"
            }
        }
    )
    return tasks
}

export async function changeStatus(task){
    //const inputId = task_id //task.id //formData.get("inputID") as string
    // const task_data = await prisma.task.findUnique({
    //     where: {
    //         id: inputId
    //     }
    // })

    const updatedStatus = !task?.isCompleted
    await prisma.task.update({
        where :{
            id: task.id
        },
        data: {
            isCompleted: updatedStatus
        }
    });
    revalidatePath("/");

    return updatedStatus;
}

export async function deleteTask(formData: FormData) {
    const inputId = formData.get("inputID") as string
    await prisma.task.delete({
        where: {
            id: inputId
        }
    })
    revalidatePath("/")
}